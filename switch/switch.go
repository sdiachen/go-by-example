package main

import (
	"fmt"
	"time"
)

func main() {
	n := 2

	switch n {
	case 1:
		fmt.Println("one")
		break
	case 2:
		fmt.Println("two")
		break
	case 3:
		fmt.Println("three")
		break
	}

	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("Weekend")
		break
	default:
		fmt.Println("Working day")
		break
	}

	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Morning")
		break
	default:
		fmt.Println("Afternoon")
		break
	}

	whatAmI := func(i interface{}) {
		switch i.(type) {
		case bool:
			fmt.Println(i, "is bool")
			break
		case int:
			fmt.Println(i, "is int")
			break
		default:
			fmt.Println(i, "is unknown")
			break
		}
	}

	whatAmI(true)
	whatAmI(13)
	whatAmI("string")
}
