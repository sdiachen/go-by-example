package main

import (
	"fmt"
	"math"
)

const a = "const"
const b string = "constB"

func main() {
	const n = 5000000

	const d = 3e20 / n
	fmt.Println(d)

	fmt.Println(int64(d))

	fmt.Println(math.Sin(n))
}
