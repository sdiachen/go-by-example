package main

import "fmt"

func main() {
	i := 1
	for i < 3 {
		fmt.Print("For1:=")
		fmt.Println(i)
		i = i + 1
	}

	for j := 3; j < 6; j++ {
		fmt.Print("For2:=")
		fmt.Println(j)
	}

	for {
		fmt.Println("loop")
		break
	}

	for n := 0; n < 10; n++ {
		if n%2 == 0 {
			continue
		}
		fmt.Print("Odd:")
		fmt.Println(n)
	}
}
