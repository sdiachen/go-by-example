package main

import "fmt"

func main() {
	var a int = 10
	fmt.Println(a)

	var b, c string = "multi", "assign"
	fmt.Println(b)
	fmt.Println(c)

	d := true
	fmt.Print(d)
}
