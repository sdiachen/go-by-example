package main

import "fmt"

func plus(a int, b int) int {
	return a + b
}

func plusPlus(a, b, c int) int {
	return a + b + c
}

func vals() (int, int) {
	return 3, 7
}

func sum(nums ...int) {
	fmt.Print(nums, " sum=")
	sum := 0
	for _, v := range nums {
		sum += v
	}
	fmt.Println(sum)
}

func intSeq() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func fact(n int) int {
	if n <= 1 {
		return 1
	}
	return n * fact(n-1)
}

func main() {
	fmt.Println("Simple functions")
	fmt.Println("1+2=", plus(1, 2))

	fmt.Println("1+2+3=", plusPlus(1, 2, 3))
	fmt.Println("==========")

	fmt.Println("Multiple return values")
	a, b := vals()
	fmt.Println("a:", a)
	fmt.Println("b:", b)

	_, v := vals()
	fmt.Println("second:", v)
	fmt.Println("==========")

	fmt.Println("Variadic functions")
	sum(1, 2)
	sum(1, 2, 3)

	s := []int{1, 3, 5, 7}
	sum(s...)
	fmt.Println("==========")

	fmt.Println("Closures")
	nextInt := intSeq()

	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())

	nextInt = intSeq()
	fmt.Println(nextInt())

	fmt.Println("==========")

	fmt.Println("Recursion")
	fmt.Println("Factorial. 7!=", fact(7))

	fmt.Println("==========")
}
