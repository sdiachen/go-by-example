package main

import (
	"fmt"
	"time"
)

func worker(done chan bool) {
	fmt.Println("Worker start")
	time.Sleep(time.Second)
	fmt.Println("Worker done")

	done <- true
}

func ping(pings chan<- string, message string) {
	pings <- message
}

func pong(pings <-chan string, pongs chan<- string) {
	msg := <-pings
	pongs <- msg
}

func main() {
	messages := make(chan string)

	go func() {
		messages <- "ping"
	}()

	msg := <-messages
	fmt.Println(msg)

	messages = make(chan string, 2)

	messages <- "buffered"
	messages <- "channel"

	fmt.Println(<-messages)
	fmt.Println(<-messages)

	done := make(chan bool)

	go worker(done)

	<-done

	pings := make(chan string, 1)
	pongs := make(chan string, 1)

	ping(pings, "passed message")
	pong(pings, pongs)
	fmt.Println(<-pongs)

	jobs := make(chan int, 5)
	done = make(chan bool)

	go func() {
		for {
			j, more := <-jobs
			if more {
				fmt.Println("Received job:", j)
			} else {
				fmt.Println("Received all jobs")
				done <- true
				return
			}
		}
	}()

	for i := 0; i < 5; i++ {
		jobs <- i
		fmt.Println("Sent job:", i)
	}
	fmt.Println("Sent all jobs")
	close(jobs)

	<-done
}
