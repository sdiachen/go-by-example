package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5}

	sum := 0
	for _, val := range nums {
		sum += val
	}
	fmt.Println("sum:", sum)

	for i, val := range nums {
		if val == 3 {
			fmt.Println("idx:", i)
		}
	}

	kvs := map[string]string{"a": "apple", "b": "banana", "c": "coconut"}
	for k, v := range kvs {
		fmt.Printf("%s -> %s\n", k, v)
	}

	for k := range kvs {
		fmt.Println("key:", k)
	}

	for i, c := range "go" {
		fmt.Println(i, c)
	}
}
